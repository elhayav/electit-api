/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.DBconnect;

import il.ac.ariel.elect_it.classes.Contestant;
import il.ac.ariel.elect_it.classes.Election;
import il.ac.ariel.elect_it.classes.ElectionAction;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author elhay87
 */
public class ElectionManag {
    
    private Connection con;
    Calendar calendarFormat ;
    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
     
    public ElectionManag(Connection connnection)
    {
        this.con = connnection;
    }
    
    public List<Election> getElectionList(ElectionAction responseObject) 
    {
        List<Election> electionListResult = new ArrayList<Election>(0);
        
         try {            
            Statement checUserkLogIn = (Statement) con.createStatement();
            ResultSet getElections = checUserkLogIn.executeQuery("SELECT * FROM `elect_it_election` WHERE ID = ANY(SELECT `electionID` FROM `elect_it_election_to_users` WHERE `userID` = "+responseObject.tempUser.getID()+")");           
            
            for(int i=1  ; getElections.next() ; i++) 
             {
                 
                 System.out.print("Selected Election "+i+".\n");
                 Election temp = new Election();
                 temp.setElectionID(getElections.getInt("ID"));
                 System.out.print("Election ID: "+temp.getElectionID()+".\n");
                 temp.setElectionName(getElections.getString("electionName"));
                 System.out.print("Election Name: "+temp.getElectionName()+".\n");
                 temp.setDescription(getElections.getString("description"));
                 System.out.print("Election Description: "+temp.getDescription()+".\n");
                 temp.setManagerID(getElections.getInt("manager"));
                 System.out.print("Election Manager ID: "+temp.getManagerID()+".\n");
                 Calendar myCalendar = Calendar.getInstance();               
                 myCalendar.setTime(getElections.getTimestamp("endElectionDate"));                 
                 temp.setEndElectionDate(myCalendar);                
                 System.out.print("Election End Date (myCalendar): "+temp.getEndElectionDate().getTime().toString()+".\n");
                 temp.setElectionImageUri(getElections.getString("electionImage"));
                 electionListResult.add(temp);
             }
            checUserkLogIn.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            electionListResult = null;
        }     
        
        return electionListResult;
    }
    public List<Election> addContestantToElections(ElectionAction responseObject) 
    {
        int electionCounter=0;
        for(Election item :  responseObject.tempElectionList)
        {
            System.out.print("\nContestant for Election "+item.getElectionName()+".\n");
            List<Contestant> contestantTempList = new ArrayList<Contestant>(0);
            
            try {            
            Statement checUserkLogIn = (Statement) con.createStatement();
            ResultSet getContestant = checUserkLogIn.executeQuery("SELECT * FROM `elect_it_contestant` WHERE election = "+item.getElectionID());           
            if(getContestant.next())
            {
                for(int contestantCounter=1  ; !getContestant.isAfterLast() ; getContestant.next(), contestantCounter++) 
                {         
                     System.out.print("Selected Contestant "+contestantCounter+".\n");
                     Contestant temp = new Contestant();
                     temp.setElectionID(getContestant.getInt("election"));
                     System.out.print("Contestant Election ID: "+temp.getElectionID()+".\n");
                     temp.setFullName(getContestant.getString("fullName"));
                     System.out.print("Contestant Name: "+temp.getFullName()+".\n");
                     temp.setDescription(getContestant.getString("description"));
                     System.out.print("Contestant Description: "+temp.getDescription()+".\n");
                     temp.setID(getContestant.getInt("ID"));
                     System.out.print("Contestant  ID: "+temp.getID()+".\n");
                     temp.setVotesCount(getContestant.getInt("votesCount"));
                     System.out.print("Contestant Votes: "+temp.getVotesCount()+".\n");
                     temp.setContestantImageUri(getContestant.getString("contestantImage"));
                     System.out.print("Contestant Image URI: "+temp.getContestantImageUri()+".\n");
                     contestantTempList.add(temp);
                 }
            }
            responseObject.tempElectionList.get(electionCounter).setContestants(contestantTempList);
            electionCounter++;
            checUserkLogIn.close();
            } catch (SQLException ex) {
                Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);           
            }     
        }
        return  responseObject.tempElectionList;
    }
    
    public boolean registerToElection(ElectionAction responseObject)
    {
        boolean registerResult = false;
        
             try 
             {
                Statement registerUserToElection = (Statement) con.createStatement();
                int registerUserToElectionResult = registerUserToElection.executeUpdate("INSERT INTO elect_it_election_to_users (`electionID` ,`userID`) VALUES ('"+responseObject.electionId+"',  '"+responseObject.tempUser.getID()+"')");
                if(registerUserToElectionResult > 0)
                {
                    System.out.print("register User To Election: Insert Seccess");
                    registerResult = true;
                }
                else
                {
                    System.out.print("register User To Election: not Seccess");
                    registerResult = false;
                }
             }            
            catch (SQLException ex) 
            {
                Logger.getLogger(ElectionManag.class.getName()).log(Level.SEVERE, null, ex);
                registerResult = false;
            }    
        return registerResult;
    }
    public boolean isElectionIdExist(ElectionAction responseObject)
    {
        boolean checkResult = false;
        
        try {
            
            Statement findIfElectionIdExist = (Statement) con.createStatement();
            ResultSet resultFindIfElectionIdExist = findIfElectionIdExist.executeQuery("SELECT EXISTS (SELECT * FROM elect_it_election WHERE  `ID` ="+responseObject.electionId+")");
            resultFindIfElectionIdExist.first();
            if(resultFindIfElectionIdExist.getInt(1) == 1)
            {          
                System.out.print("ID Exist");
                checkResult = true;
            }
            else
            {
                System.out.print("ID Not Exist");
                checkResult = false;
            }
            findIfElectionIdExist.close();
        } catch (SQLException ex) {
            Logger.getLogger(ElectionManag.class.getName()).log(Level.SEVERE, null, ex);
            checkResult = false;
        }
        return checkResult;
    }
    
    public boolean isalreadyRegistered(ElectionAction responseObject)
    {
        boolean checkResult = false;
        
        try {
            
            Statement findIfElectionIdExist = (Statement) con.createStatement();
            ResultSet resultFindIfElectionIdExist = findIfElectionIdExist.executeQuery("SELECT EXISTS (SELECT * FROM `elect_it_election_to_users` WHERE  electionID ='"+responseObject.electionId+"' AND `userID` = '"+responseObject.tempUser.getID()+"')");
            resultFindIfElectionIdExist.first();
            if(resultFindIfElectionIdExist.getInt(1) == 1)
            {          
                System.out.print("User already Registered To this Election");
                checkResult = true;
            }
            else
            {
                System.out.print("User Not Registered To this Election");
                checkResult = false;
            }
            findIfElectionIdExist.close();
        } catch (SQLException ex) {
            Logger.getLogger(ElectionManag.class.getName()).log(Level.SEVERE, null, ex);
            checkResult = false;
        }
        return checkResult;
    }
    public boolean removFromElection(ElectionAction responseObject)
    {
            boolean removResult = false;
            int resultFindIfElectionIdExistInt;
            Statement findIfElectionIdExist;
            if(responseObject.tempUser != null && responseObject.electionId != 0)
            {
                int userVoteInThisElectionToContestantID = 0;
                
                for(Contestant itemContestant : responseObject.tempElection.getContestants())
                {
                    for(int itemUserVoteList : responseObject.tempUser.getVoteTo())
                    {  
                        if(itemContestant.getID() == itemUserVoteList)
                        {
                            userVoteInThisElectionToContestantID = itemUserVoteList;
                            break;
                        }
                    }
                    if(userVoteInThisElectionToContestantID != 0)
                        break;
                }

                try 
                {
                    findIfElectionIdExist = (Statement) con.createStatement();
                    System.out.println("Contestant to sub vote: "+userVoteInThisElectionToContestantID);
                    if(userVoteInThisElectionToContestantID != 0 && responseObject.tempElection.getEndElectionDate().after(Calendar.getInstance()))
                    {
                        resultFindIfElectionIdExistInt = findIfElectionIdExist.executeUpdate("UPDATE `elect_it_contestant` SET `votesCount` = `votesCount`-1 WHERE ID = "+userVoteInThisElectionToContestantID);
                        if(resultFindIfElectionIdExistInt > 0)
                        {
                            System.out.println("-- from contestant ID: "+userVoteInThisElectionToContestantID+" success");
                        }
                        else
                        {
                            System.out.println("-- from contestant ID: "+userVoteInThisElectionToContestantID+" NOT success");
                        }
                    }
                    resultFindIfElectionIdExistInt = findIfElectionIdExist.executeUpdate("DELETE FROM `elect_it_election_to_users` WHERE `userID`="+responseObject.tempUser.getID()+" AND `electionID`="+responseObject.electionId);
                    if(resultFindIfElectionIdExistInt > 0)
                    {
                       System.out.println("remov electionID : "+responseObject.electionId+". from User: "+ responseObject.tempUser.getFirstName() +" success");
                       removResult = true;

                    }             
                } 
                catch (SQLException ex) 
                {
                    Logger.getLogger(ElectionManag.class.getName()).log(Level.SEVERE, null, ex);
                    removResult = false;
                }
            }
    

        return removResult;
    }
}
