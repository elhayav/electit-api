/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.DBconnect;

import il.ac.ariel.elect_it.classes.ContestantAction;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author elhay87
 */
public class ContestantManag {
    
    private Connection con;

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
     
    public ContestantManag(Connection connnection)
    {
        this.con = connnection;
    }
    
    public boolean UserVoteStutus(ContestantAction responseObject)
    {
        boolean resoaltOfUserVoteStutus = false;
        try {
            Statement getUserVote = (Statement) con.createStatement();
            ResultSet getUserVoteResult = getUserVote.executeQuery("SELECT `voteTo` FROM `elect_it_election_to_users` WHERE `userID` = "+responseObject.tempUser.getID()+" AND `electionID`="+responseObject.tempContestant.getElectionID());
            getUserVoteResult.next();
            if(getUserVoteResult.getInt(1) == responseObject.tempContestant.getID())
            {
                System.out.println("User vote to Contestant ID: "+getUserVoteResult.getInt(1)+" And tray to vote To: "+responseObject.tempContestant.getID());
                resoaltOfUserVoteStutus = false;
            }
            else
            {
                System.out.println("Old Vote: "+getUserVoteResult.getInt(1));
                resoaltOfUserVoteStutus = true;
            }
        } catch (SQLException ex) {
            
            Logger.getLogger(ContestantManag.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resoaltOfUserVoteStutus;
    }
    
    public boolean voteToContestant(ContestantAction responseObject)
    {
        boolean resoaltOfVote = false;
        try {
            Statement getUserVote = (Statement) con.createStatement();
            ResultSet getUserVoteResult = getUserVote.executeQuery("SELECT `voteTo` FROM `elect_it_election_to_users` WHERE `userID` = "+responseObject.tempUser.getID()+" AND `electionID`="+responseObject.tempContestant.getElectionID());
            getUserVoteResult.next();
            if(getUserVoteResult.getInt(1) != 0)
            {
                System.out.println("OLD VOTE TO contestant ID: "+getUserVoteResult.getString(1));
                int deleteOldVote = getUserVote.executeUpdate("UPDATE `elect_it_contestant` SET `votesCount` = `votesCount`-1 WHERE ID = "+getUserVoteResult.getString(1));
                if(deleteOldVote > 0)
                {
                    System.out.println("Remov old vote Success");
                    int newVote = getUserVote.executeUpdate("UPDATE `elect_it_contestant` SET `votesCount` = `votesCount`+1 WHERE ID = "+responseObject.tempContestant.getID());
                    if(newVote > 0)
                    {
                        System.out.println("Contestant ID: "+responseObject.tempContestant.getID()+" ++ Success");
                        int updateUser = getUserVote.executeUpdate("UPDATE `elect_it_election_to_users` SET `voteTo` = "+responseObject.tempContestant.getID()+" WHERE `userID` = "+responseObject.tempUser.getID()+" AND `electionID`="+responseObject.tempContestant.getElectionID());
                        if(updateUser > 0)
                        {
                            System.out.println("User vote update Success");
                            resoaltOfVote = true;
                        }
                        else
                        {
                            System.out.println("User vote update NOT Success");
                            resoaltOfVote = false;
                        }
                        
                    }
                    else
                    {
                        System.out.println("Update new vote NOT Success");
                        resoaltOfVote = false;
                    }
                }
                else
                {
                    System.out.println("Remov old vote NOT Success");
                    resoaltOfVote = false;
                }
            }
            else
            {
                    System.out.println("THIS IS YOUR FIRST VOTE for this user in this election");
                    int newVote = getUserVote.executeUpdate("UPDATE `elect_it_contestant` SET `votesCount` = `votesCount`+1 WHERE ID = "+responseObject.tempContestant.getID());
                    if(newVote > 0)
                    {
                        System.out.println("Contestant ++ Success");
                        int updateUser = getUserVote.executeUpdate("UPDATE `elect_it_election_to_users` SET `voteTo` = "+responseObject.tempContestant.getID()+" WHERE `userID` = "+responseObject.tempUser.getID()+" AND `electionID`="+responseObject.tempContestant.getElectionID());
                        if(updateUser > 0)
                        {
                            System.out.println("User vote update Success");
                            resoaltOfVote = true;
                        }
                        else
                        {
                            System.out.println("User vote update NOT Success");
                            resoaltOfVote = false;
                        }
                    }
                    else
                    {
                        System.out.println("Update new vote NOT Success");
                        resoaltOfVote = false;
                    }
            }
        } catch (SQLException ex) {
            
            Logger.getLogger(ContestantManag.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return resoaltOfVote;
    }
    public List<Integer> getAllUserVotes(int id)
    {
        
        List<Integer> idIntList = new ArrayList<Integer>(0) ;
        
        try {
            Statement getAllvotes = (Statement) con.createStatement();
            ResultSet getAllVotesResult = getAllvotes.executeQuery("SELECT `voteTo` FROM `elect_it_election_to_users` WHERE `userID` = "+id);
            
            while(getAllVotesResult.next())
            {
                idIntList.add(getAllVotesResult.getInt(1));                        
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ContestantManag.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return idIntList;
    }
}
