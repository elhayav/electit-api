/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.DBconnect;

import il.ac.ariel.elect_it.classes.User;
import il.ac.ariel.elect_it.classes.UserAction;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import javax.imageio.ImageIO;

/**
 *
 * @author elhay87
 */
public class UserManag {
    private Connection con;
    
   

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
     
    public UserManag(Connection connnection)
    {
        this.con = connnection;
        
    }
    /**
     * insert new user to data base to `elect_it_users` table.
     * and return boolean answer
     * 
     * @param responseObject
     * @return boolean 
     */
    public boolean addUser(UserAction responseObject)
    {
        boolean insertResult = false;
        //new user get temporary random password 
        Random intRandomPassword = new Random();
        String stringRandomPassword = Integer.toString(intRandomPassword.nextInt(9999));
        
        try {            
            Statement insertNewUser = (Statement) con.createStatement();
            int updatResult = insertNewUser.executeUpdate("INSERT into elect_it_users (`firstName`, `eMail`, `password`, `userProfileImg`) VALUES('"+responseObject.tempUser.getFirstName()+"', '"+responseObject.tempUser.geteMail()+"','"+stringRandomPassword+"','default.png')");           
            insertNewUser.close();
            if(updatResult>0)
            {
                insertResult = true;
            }
            else
            {
                insertResult = false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            insertResult = false;
        }                    
        return insertResult;
    }
    public boolean checkLogIn(UserAction responseObject)
    {
        boolean logInResult = false;
        
        try {            
            Statement checUserkLogIn = (Statement) con.createStatement();
            ResultSet loginresult = checUserkLogIn.executeQuery("SELECT * FROM `elect_it_users` WHERE eMail = '"+responseObject.tempUser.geteMail()+"' AND PASSWORD = '"+responseObject.tempUser.getPassword()+"'");           
            if(loginresult.first())
            {
                System.out.print("\neMail: "+loginresult.getString("eMail")+", Password: "+loginresult.getString("password")+"\n");            
                logInResult = true;
            }
            checUserkLogIn.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            logInResult = false;
        }              
        return logInResult;
    }
    public User getUser(UserAction responseObject)
    {
        User returnUser = null;
        
         try {            
            Statement checUserkLogIn = (Statement) con.createStatement();
            ResultSet getUser = checUserkLogIn.executeQuery("SELECT * FROM `elect_it_users` WHERE eMail = '"+responseObject.tempUser.geteMail()+"' AND PASSWORD = '"+responseObject.tempUser.getPassword()+"'");           
            getUser.first();
            if(getUser.isLast())
            {
                returnUser = new User();
                returnUser.setID(getUser.getInt("ID"));
                System.out.print("ID: "+returnUser.getID()+".\n");
                returnUser.setFirstName(getUser.getString("firstName"));
                System.out.print("FirstName: "+returnUser.getFirstName()+".\n");
                returnUser.setLastName(getUser.getString("lastName"));
                System.out.print("LastName: "+returnUser.getLastName()+".\n");
                returnUser.setPassword(getUser.getString("password"));
                System.out.print("Password: "+returnUser.getPassword()+".\n");
                returnUser.setPhone(getUser.getString("phone"));
                System.out.print("Phone: "+returnUser.getPhone()+".\n");
                //Image from db
                returnUser.setUserProfileImageUri(getUser.getString("userProfileImg"));
                returnUser.seteMail(getUser.getString("eMail"));
                System.out.print("Email: "+returnUser.geteMail()+".\n");                
                returnUser.setVoteTo(getAllUserVotes(returnUser.getID()));
                System.out.print("voteTo: "+returnUser.getVoteTo().toString()+".\n");
            }
            checUserkLogIn.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            returnUser = null;
        }        
        return returnUser;    
    }
    public List<Integer> getAllUserVotes(int id)
    {
        
        List<Integer> idIntList = new ArrayList<Integer>(0) ;
        
        try {
            Statement getAllvotes = (Statement) con.createStatement();
            ResultSet getAllVotesResult = getAllvotes.executeQuery("SELECT `voteTo` FROM `elect_it_election_to_users` WHERE `userID` = "+id);
            
            while(getAllVotesResult.next())
            {
                idIntList.add(getAllVotesResult.getInt(1));                        
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ContestantManag.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return idIntList;
    }
   public boolean updateUser(UserAction responseObject)
    {
        boolean updatetResult = false;
        
        //funcation that converts bitmap to file and saves it on server and returns the file's uri.
       
        
        try {            
            Statement insertNewUser = (Statement) con.createStatement();
            int updatResult = insertNewUser.executeUpdate("UPDATE `elect_it_users` SET `firstName`='"+responseObject.tempUser.getFirstName()+"',`lastName`='"+responseObject.tempUser.getLastName()+"',`phone`='"+responseObject.tempUser.getPhone()+"',`password`='"+responseObject.tempUser.getPassword()+"' WHERE ID = "+responseObject.tempUser.getID());           
            insertNewUser.close();
            if(updatResult>0)
            {
                System.out.print("Update User Seccess!");
                updatetResult = true;
            }
            else
            {
                System.out.print("Update User NOT Seccess!");
                updatetResult = false;
            }
            insertNewUser.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("Update User NOT Seccess!");
            updatetResult = false;
        }                    
        return updatetResult;
    }
   
   public boolean updateUserImage(UserAction responseObject, String path)
   {
        boolean updatetResult = false;
        String userID = Integer.toString(responseObject.tempUser.getID());
        String URI = SaveUserProfileImageOnServer(responseObject.bitmapAsByteArry,path, userID);
        String[] URIarray = URI.split("/");
        responseObject.ImageUri = URIarray[ URIarray.length - 1 ];
        
        try {            
            Statement insertNewUser = (Statement) con.createStatement();
            int updatResult = insertNewUser.executeUpdate("UPDATE `elect_it_users` SET `userProfileImg`='"+responseObject.ImageUri.toString()+"' WHERE ID = "+responseObject.tempUser.getID());           
            insertNewUser.close();
            if(updatResult>0)
            {
                System.out.print("Update User Seccess!");
                updatetResult = true;
            }
            else
            {
                System.out.print("Update User NOT Seccess!");
                updatetResult = false;
            }
            insertNewUser.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("Update User NOT Seccess!");
            updatetResult = false;
        }                    
        return updatetResult;
   }
   
   public String SaveUserProfileImageOnServer(byte[] userImageByteArray, String fileName, String userID)
   {
       
        try {
            final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(userImageByteArray));
            if(bufferedImage !=null)
                
                ImageIO.write(bufferedImage, "png", new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userID + ".png";
   }
   
   
   public boolean sendPassToMail(UserAction responseObject) throws IOException
   {
       User returnUser = null;
       boolean sendResult = false; 
         try {            
            Statement checUserkLogIn = (Statement) con.createStatement();
            ResultSet getUser = checUserkLogIn.executeQuery("SELECT * FROM `elect_it_users` WHERE eMail = '"+responseObject.tempUser.geteMail()+"'");           
             getUser.first();
            if(getUser.isLast())
            {
                returnUser = new User();
                returnUser.setID(getUser.getInt("ID"));
                System.out.print("ID: "+returnUser.getID()+".\n");
                returnUser.setFirstName(getUser.getString("firstName"));
                System.out.print("FirstName: "+returnUser.getFirstName()+".\n");
                returnUser.setLastName(getUser.getString("lastName"));
                System.out.print("LastName: "+returnUser.getLastName()+".\n");
                returnUser.setPassword(getUser.getString("password"));
                System.out.print("Password: "+returnUser.getPassword()+".\n");
                returnUser.setPhone(getUser.getString("phone"));
                System.out.print("Phone: "+returnUser.getPhone()+".\n");
                /*InputStream  blobToInputStream = getUser.getBlob("userProfileImg").getBinaryStream();
                returnUser.setUserProfileImage(BitmapFactory.decodeStream(blobToInputStream));*/
                returnUser.seteMail(getUser.getString("eMail"));
                System.out.print("Email: "+returnUser.geteMail()+".\n");
                //returnUser.setVoteTo(getUser.getInt("voteTo"));
                //System.out.print("voteTo: "+returnUser.getVoteTo()+".\n");
                System.out.print("=================+=========>\n");
            }
            checUserkLogIn.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
            returnUser = null;
            return sendResult;
        } 
         System.out.print("before Try to send mail.\n");
         if(returnUser != null)
         {
             System.out.print("Try to send mail.\n");
             try{
                 
             
                String pName = URLEncoder.encode(returnUser.getFirstName(), "UTF-8");
                String eMail = URLEncoder.encode(returnUser.geteMail(), "UTF-8");

                System.out.print("Send mail to: http://exemple.elhay.co.il/php/electitmail.php?email="+eMail+"&pass="+returnUser.getPassword()+"&pname="+pName+"&lname=null");
                URL url = new URL("http://exemple.elhay.co.il/php/electitmail.php?email="+eMail+"&pass="+returnUser.getPassword()+"&pname="+pName+"&lname=null");
               InputStream is = url.openConnection().getInputStream();

               BufferedReader reader = new BufferedReader( new InputStreamReader( is )  );

               System.out.print("Mail server response.\n");
               String line = null;
               while( ( line = reader.readLine() ) != null )  
               {
                  System.out.println(line);
                  if(line.contains("<pre>!send mail succeed!<pre>"))
                  {
                      sendResult = true;
                      break;
                  }
               }
               reader.close();
            }catch(Exception ex){ 
                Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
                sendResult = false;
                return sendResult;
            }
            catch(Error ex){ 
                Logger.getLogger(UserManag.class.getName()).log(Level.SEVERE, null, ex);
                sendResult = false;
                return sendResult;
            }
         }
         else{
             System.out.print("return user == null.\n");
             return sendResult;
         }
         return sendResult;
   }
}
