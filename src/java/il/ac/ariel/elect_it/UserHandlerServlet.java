/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import il.ac.ariel.elect_it.classes.UserAction;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;
import il.ac.ariel.elect_it.DBconnect.UserManag;
import il.ac.ariel.elect_it.classes.Action;
import java.util.Properties;

/**
 * this servlet handles to connet between aplication and data base
 * to manage user table
 * 
 */
@WebServlet(name = "ElectItServer", urlPatterns = {"/UserHandlerServlet"})
public class UserHandlerServlet extends HttpServlet {
    //jdbc connection string
    Connection connectToDB;
    Properties properties;
    String url;
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);       
        properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "");
        properties.put("characterEncoding", "UTF-8");
        properties.put("useUnicode", "true");
        url = "jdbc:mysql://localhost:3306/electit_database?characterEncoding=utf8";
        
       
    }
    @Override
    public void destroy() {
    try {
        super.destroy();
        if(connectToDB != null && !connectToDB.isClosed())
            connectToDB.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserHandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    { 
        
        try
        {
            if(connectToDB == null || !connectToDB.isValid(5))
            {
                Class.forName("com.mysql.jdbc.Driver");
                connectToDB = DriverManager.getConnection(url, properties);
            }
        }
        catch(Exception e)
        {
            Logger.getLogger(ElectionHandlerServlet.class.getName()).log(Level.SEVERE, null, e);
        }
        response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        UserManag userMenag  = new UserManag(connectToDB);       
        UserAction responseObject = new UserAction();
        PrintWriter out = response.getWriter();
        responseObject = getRequestObject(request);              
        switch(responseObject.getOperationCode())
        {             
             case Action.CHECK_SERVER:
             {
                 System.out.println("CHECK_SERVER");
                 responseObject.setResponseStatus("connected");
                 responseObject.setBooleanResponseStatus(true);               
                 break;
             }
             case UserAction.REGISTER_USER:
             {
                if(userMenag.addUser(responseObject) == false)
                {
                    
                    System.out.println("INSERT not sccede");
                    responseObject.setResponseStatus("ההרשמה נכשלהת נסה שנית מאוחר יותר");
                    responseObject.setBooleanResponseStatus(false);
                }
                else
                {
                   System.out.println("INSERT sccede");
                   if(userMenag.sendPassToMail(responseObject) == true)
                   {
                       System.out.println("Password mail sent");
                       responseObject.setResponseStatus("ההרשמה הסתיימה בהצלחה\n סיסמה נשלחה למייל שהזנת.");
                       responseObject.setBooleanResponseStatus(true);
                   }
                   else
                   {
                       System.out.println("Password mail not sent");
                       responseObject.setResponseStatus("ההרשמה לא הצליחה נסה שוב בפעם אחרת או בדוק את הפרטים שנית");
                       responseObject.setBooleanResponseStatus(false);
                   } 
                } 
                break;
             }
             case UserAction.CHECK_LOG_IN:
             {               
                if(userMenag.checkLogIn(responseObject))
                {
                    System.out.println("Log In success");
                    responseObject.setResponseStatus("Log In success");
                    responseObject.setBooleanResponseStatus(true);
                    responseObject.tempUser = userMenag.getUser(responseObject);                    
                }
                else
                {
                   System.out.println("Log In not sccede");
                   responseObject.setResponseStatus("Log In not sccede");
                   responseObject.setBooleanResponseStatus(false);
                } 
                break;
             }
             case UserAction.UPDATE_USER:
             {
                 if(userMenag.updateUser(responseObject))
                 {
                     responseObject.setBooleanResponseStatus(true);
                     responseObject.setResponseStatus("Update User Seccess!");
                 }
                 else
                 {
                     responseObject.setBooleanResponseStatus(false);
                     responseObject.setResponseStatus("Update User NOT Seccess!");
                 }
                 break;
             }
                 
             case UserAction.UPDATE_USER_IMAGE:
             {
                 //String localPath = getServletContext().getRealPath("/images/userImage/"+responseObject.tempUser.getID()+".png");
                 String localPath = "/opt/bitnami/apache-tomcat/webapps/images/userImage/"+responseObject.tempUser.getID()+".png";
                 if(userMenag.updateUserImage(responseObject, localPath))
                 {
                     responseObject.setBooleanResponseStatus(true);
                     responseObject.setResponseStatus("Update User Seccess!");
                 }
                 else
                 {
                     responseObject.setBooleanResponseStatus(false);
                     responseObject.setResponseStatus("Update User NOT Seccess!");
                 }
                 break;
             }
         }
        //send information back to aplication
        out.print(createServerResponse(responseObject));
        try {
        super.destroy();
        if(!connectToDB.isClosed())
            connectToDB.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserHandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    } 
    /**
     * convert request from json String to UserAction Object.
     * 
     * @param request
     * @return
     * @throws IOException 
     */
    public UserAction getRequestObject(HttpServletRequest request) throws IOException
    {
	//String personDetails = request.getParameter("newPerson");
	StringBuilder sb = new StringBuilder();
	BufferedReader br = request.getReader();
	String objectStr, str;
	while( (str = br.readLine()) != null ){
	    sb.append(str);
	}

        objectStr = sb.toString();
	Gson gsonConvertFrom = new Gson();
        System.out.println("RequestObject:\n"+objectStr);
	UserAction receivedAction = gsonConvertFrom.fromJson(objectStr, UserAction.class);

	return receivedAction;
    }
    /**
     * convert UserAction Object to String.
     * 
     * @param responseObject
     * @return 
     */
    public String createServerResponse(UserAction responseObject)
    {
	String response;
	try
	{
	 response = toString(responseObject);
	}catch(JSONException e)
	{
	    response = "";
	}       
	return response;
    }
    /**
     * convert UserAction Object to String in json format.
     * 
     * @param responseObject
     * @return
     * @throws JSONException 
     */
    public String toString(UserAction responseObject) throws JSONException
    {
	    GsonBuilder build = new GsonBuilder();
            //build.setDateFormat("yyyy-MM-dd");
            //build.setDateFormat("yyyy-MM-dd");
            
            build.registerTypeAdapter(Date.class, new DateTypeAdapter())
            .registerTypeAdapter(java.sql.Date.class, new DateTypeAdapter());
		
            Gson gsonConvertFrom = build.create();
            Gson gsonConvertTo = build.create();
            
	    String jsonString = gsonConvertTo.toJson(responseObject);
            System.out.println("ResponseObject:\n"+jsonString);
            //JSONObject myJsonObj = new JSONObject(jsonString);
            return jsonString;
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}