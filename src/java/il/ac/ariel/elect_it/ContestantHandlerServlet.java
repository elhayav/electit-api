/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.DateTypeAdapter;
import il.ac.ariel.elect_it.DBconnect.ContestantManag;
import il.ac.ariel.elect_it.classes.Action;
import il.ac.ariel.elect_it.classes.ContestantAction;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONException;

/**
 *
 * @author elhay87
 */
@WebServlet(name = "ElectItServer", urlPatterns = {"/ContestantHandlerServlet"})
public class ContestantHandlerServlet extends HttpServlet {

     //jdbc connection string
    Connection connectToDB;
    Properties properties;
    String url;
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);       
        properties = new Properties();
        properties.put("user", "root");
        properties.put("password", "");
        properties.put("characterEncoding", "UTF-8");
        properties.put("useUnicode", "true");
        url = "jdbc:mysql://localhost:3306/electit_database?characterEncoding=utf8";
        
    }
    @Override
    public void destroy() {
    try {
        super.destroy();
        if(connectToDB != null && !connectToDB.isClosed())
            connectToDB.close();
        } catch (SQLException ex) {
            Logger.getLogger(ElectionHandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try
        {
            if(connectToDB == null || !connectToDB.isValid(5))
            {
                Class.forName("com.mysql.jdbc.Driver");
                connectToDB = DriverManager.getConnection(url, properties);
            }
        }
        catch(Exception e)
        {
            Logger.getLogger(ElectionHandlerServlet.class.getName()).log(Level.SEVERE, null, e);
        }
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/text; charset=UTF-8");
        ContestantManag contestantManag  = new ContestantManag(connectToDB);       
        ContestantAction responseObject = new ContestantAction();
        PrintWriter out = response.getWriter();
        responseObject = getRequestObject(request);              
        switch(responseObject.getOperationCode())
        {             
             case Action.CHECK_SERVER:
             {
                 System.out.println("CHECK_SERVER\n");
                 responseObject.setResponseStatus("connected");
                 responseObject.setBooleanResponseStatus(true);               
                 break;
             }
             case ContestantAction.VOTE_TO_CONTESTANT:
             {
                 if(contestantManag.UserVoteStutus(responseObject))
                 {
                     if(contestantManag.voteToContestant(responseObject))
                     {
                         responseObject.tempUser.setVoteTo(contestantManag.getAllUserVotes(responseObject.tempUser.getID()));
                         responseObject.setBooleanResponseStatus(true);
                         responseObject.setResponseStatus("VOTE saved");
                     }
                     else
                     {
                         responseObject.setBooleanResponseStatus(false);
                         responseObject.setResponseStatus("VOTE NOT saved");
                     }
                 }
                 else
                 {
                     responseObject.setBooleanResponseStatus(false);
                     responseObject.setResponseStatus("you olredy vote to this contestant");
                 }
                 break;
             }
         }
        //send information back to aplication
        out.print(createServerResponse(responseObject));
        try {
        super.destroy();
        if(!connectToDB.isClosed())
            connectToDB.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserHandlerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * convert request from json String to ContestantAction Object.
     * 
     * @param request
     * @return
     * @throws IOException 
     */
    public ContestantAction getRequestObject(HttpServletRequest request) throws IOException
    {
	//String personDetails = request.getParameter("newPerson");
	StringBuilder sb = new StringBuilder();
	BufferedReader br = request.getReader();
	String objectStr, str;
	while( (str = br.readLine()) != null ){
	    sb.append(str);
	}

        objectStr = sb.toString();
	Gson gsonConvertFrom = new Gson();
        System.out.println("RequestObject:\n"+objectStr);
	ContestantAction receivedAction = gsonConvertFrom.fromJson(objectStr, ContestantAction.class);

	return receivedAction;
    }
    /**
     * convert ContestantAction Object to String.
     * 
     * @param responseObject
     * @return 
     */
    public String createServerResponse(ContestantAction responseObject)
    {
	String response;
	try
	{
            response = toString(responseObject);
	}catch(JSONException e)
	{
	    response = "";
	}       
	return response;
    }
    /**
     * convert ContestantAction Object to String in json format.
     * 
     * @param responseObject
     * @return
     * @throws JSONException 
     */
    public String toString(ContestantAction responseObject) throws JSONException
    {
	    
            GsonBuilder build = new GsonBuilder();
            //build.setDateFormat("yyyy-MM-dd");
            //build.setDateFormat("yyyy-MM-dd");
            
            build.registerTypeAdapter(Date.class, new DateTypeAdapter())
            .registerTypeAdapter(java.sql.Date.class, new DateTypeAdapter());
		
            Gson gsonConvertFrom = build.create();
            Gson gsonConvertTo = build.create();
            
            
            
	    String jsonString = gsonConvertTo.toJson(responseObject);
            System.out.println("ResponseObject:\n"+jsonString);
            
            return jsonString;
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
