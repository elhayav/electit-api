/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.classes;

/**
 *
 * @author elhay87
 */
public class Action {
	
    public static final int CHECK_SERVER =  0;	
	private int operationCode;	
	private String responseStatus;
	private boolean booleanResponseStatus;
	
	public int getOperationCode() {
		return operationCode;
	}
	public void setOperationCode(int operationCode) {
		this.operationCode = operationCode;
	}
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public boolean isBooleanResponseStatus() {
		return booleanResponseStatus;
	}
	public void setBooleanResponseStatus(boolean booleanResponseStatus) {
		this.booleanResponseStatus = booleanResponseStatus;
	}    
}
