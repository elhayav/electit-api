/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.classes;

/**
 *
 * @author elhay87
 */
public class Contestant {
    private int ID;
    private String fullName;
    private String description;
    private int electionID;
    private int votesCount;
    private String contestantImageUri;

    public String getContestantImageUri() {
        return contestantImageUri;
    }

    public void setContestantImageUri(String contestantImageUri) {
        this.contestantImageUri = contestantImageUri;
    }

    public int getID() {
            return ID;
    }
    public void setID(int iD) {
            ID = iD;
    }
    public String getFullName() {
            return fullName;
    }
    public void setFullName(String fullName) {
            this.fullName = fullName;
    }
    public String getDescription() {
            return description;
    }
    public void setDescription(String description) {
            this.description = description;
    }
    public int getElectionID() {
            return electionID;
    }
    public void setElectionID(int electionID) {
            this.electionID = electionID;
    }
    public int getVotesCount() {
            return votesCount;
    }
    public void setVotesCount(int votesCount) {
            this.votesCount = votesCount;
    }
   /* public Bitmap getContestantImage() {
            return contestantImage;
    }
    public void setContestantImage(Bitmap contestantImage) {
            this.contestantImage = contestantImage;
    }  
    */
}
