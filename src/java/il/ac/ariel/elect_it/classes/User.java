/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package il.ac.ariel.elect_it.classes;
import android.graphics.Bitmap;
import java.util.List;
/**
 *
 * @author elhay87
 */
public class User {
    private int ID;
    private String firstName;
	private String lastName;
	private String phone;
	private String eMail;
    private List<Integer> voteTo;
	private String password;
	private String userProfileImageUri;
	
	public User(String firstName, String eMail)
	{
		setFirstName(firstName);
		seteMail(eMail);
	}
	public User()
        {
            
        }
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserProfileImageUri() {
		return userProfileImageUri;
	}

	public void setUserProfileImageUri(String userProfileImageUri) {
		this.userProfileImageUri = userProfileImageUri;
	}
        public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
        
        public List<Integer> getVoteTo() {
            return voteTo;
        }

        public void setVoteTo(List<Integer> voteTo) {
            this.voteTo = voteTo;
        }
}
