/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package il.ac.ariel.elect_it.classes;

import java.util.Calendar;
import java.util.List;

/**
 *
 * @author elhay87
 */
public class Election {
    private int electionID;
    private int managerID;
    private String electionName;
    private String Description;	
    private Calendar endElectionDate = Calendar.getInstance();
    private List<Contestant> contestants;
    private String electionImageUri;

    public int getElectionID() {
            return electionID;
    }
    public void setElectionID(int electionID) {
            this.electionID = electionID;
    }
    public int getManagerID() {
            return managerID;
    }
    public void setManagerID(int managerID) {
            this.managerID = managerID;
    }
    public String getDescription() {
            return Description;
    }
    public void setDescription(String description) {
            Description = description;
    }
   /* public Bitmap getElectionImage() {
            return electionImage;
    }
    public void setElectionImage(Bitmap electionImage) {
            this.electionImage = electionImage;
    }
    */
    public Calendar getEndElectionDate() {
            return endElectionDate;
    }
    public void setEndElectionDate(Calendar endElectionDate) {
            this.endElectionDate = endElectionDate;
    }
    public String getElectionName() {
            return electionName;
    }
    public void setElectionName(String electionName) {
            this.electionName = electionName;
    }
    public List<Contestant> getContestants() {
            return contestants;
    }
    public void setContestants(List<Contestant> contestants) {
            this.contestants = contestants;
    }       

    /**
     * @return the electionImageUri
     */
    public String getElectionImageUri() {
        return electionImageUri;
    }

    /**
     * @param electionImageUri the electionImageUri to set
     */
    public void setElectionImageUri(String electionImageUri) {
        this.electionImageUri = electionImageUri;
    }
}
